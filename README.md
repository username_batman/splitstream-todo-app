# Splitstream-ToDo-App

Assignment Tasks.

1. A landing page with a Option to create a ToDo list or View/Edit existing to do list
2. Each Todo List Must consist of the following details
- Name - Required
- Date and Time - Optional
- Reminder Required - Optional and should be allowed to be set only if the Date and Time has been set
- Importance (High, Medium and Low) - Optional
- Note* - If Date And Time are set in a Todo List, it must apply to all tasks inside the todo list
3. The user should be able to create Multiple Todo Lists
4. Users should be able to add, edit or delete tasks on to any Todo List
5. Each Task that the user creates with a ToDo list has to be described by the following details
- Name - Required
- Status (Done/Pending) - Required - Defaults to false(pending) - Indicates whether the task has been completed or not
- Importance - Optional
- Date and Time - Optional
- Reminder Required - Optional and should be allowed to be set only if the Date and Time has been set
6. Let Users Mark a task as completed




